﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VehicleInsuranceManagement.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult About()
        {
            return View();
        }
        public ActionResult InsuranceAll()
        {
            return View();
        }
        public ActionResult InsuranceSingle()
        {
            return View();
        }
        public ActionResult MakeAClaim()
        {
            return View();
        }
    }
}